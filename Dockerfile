FROM openjdk:8u212-jdk-alpine3.9

# 设置时区
ENV TZ=Asia/Shanghai
RUN set -eux; \ 
 sed -i 's/dl-cdn.alpinelinux.org/mirrors.ustc.edu.cn/g' /etc/apk/repositories; \
 apk add --no-cache --update tzdata curl fontconfig ttf-dejavu busybox-extras; \
 ln -snf /usr/share/zoneinfo/$TZ /etc/localtime; \
 echo $TZ > /etc/timezone;

# 增加arthas，如果不需要arthas，则后面的语句没有用了
COPY --from=hengyunabc/arthas:latest /opt/arthas /opt/arthas
# 占用PID=1
RUN apk add --no-cache tini
# Tini is now available at /sbin/tini
ENTRYPOINT ["/sbin/tini", "--"]