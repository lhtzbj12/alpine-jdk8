# alpine-jdk8

#### 介绍
基于alpine集成openjdk 212 的java运行环境镜像，增加了时区，并支持arthas。

#### 使用说明

1. 此镜像已提交至阿里云

   ```
   registry.cn-beijing.aliyuncs.com/lhtzbj12/alpine-jdk8
   ```

   

2. 基于本镜像，打包自己的应用

   ```
   FROM registry.cn-beijing.aliyuncs.com/lhtzbj12/alpine-jdk8
   XXXXXX
   ```

   

3. 运行容器后，使用arthas

   ```
   docker exec -it ${containerId} java -jar /opt/arthas/arthas-boot.jar
   ```

   
